# README #

### What is this repository for? ###


[//]: <> (your comment goes here and here)

[//]: <> (This is also a comment.)



[//]: <> (* Quick summary)
[//]: <> (* Version)
[//]: <> ( * [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo ) 


Extremely simple git file change trigger detection 
 
* Step 1. Gets files from $gitrepo into $gitdir
* Step 2. Compares the time for $triggerFile and $triggerReference
* Step 3. On recent change RESTARTS DATA DIODE and updates $triggerReference
* Step 4. No recent change exits gracefully



### How do I get set up? ###

git clone https://nunocruz@bitbucket.org/nunocruz/git-trigger.git

### How do I configure my trigger file and action? ###

Edit file git-trigger.sh to set your $triggerFile and the action on trigger ($triggerFile change detected).

### How do I run the trigger detection? ###

sh git-trigger.sh

* This script should be run every XX minutes using a linux Cron Job 


### Who do I talk to? ###

email: nunocruz@ipfn.tecnico.ulisboa.pt
