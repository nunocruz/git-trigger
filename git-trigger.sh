#!/bin/sh

# Extremely simple git file change trigger detection 
# 
# Step 1. Gets files from $gitrepo into $gitdir
# Step 2. Compares the time for $triggerFile and $triggerReference
# Step 3. On recent change RESTARTS DATA DIODE and updates $triggerReference
# Step 4. No recent change exits gracefully

# This script should be run every XX minutes using a linux Cron Job s


# configxmlfile=PV-config.xml
# configdbfile=PV-config.db
triggerFile=PV-diode.trigger
triggerReference=PV-diode.trigger.reference

gitrepo='https://nunocruz@bitbucket.org/nunocruz/git-trigger.git'
gitdir=~/projects/git-trigger


#echo Touching files
#touch $configxmlfile
#touch $configdbfile
#touch $triggerfile

echo Checking $gitrepo for changes

cd $gitdir
git pull

REFFILE=$triggerReference
FILE=$triggerFile
# How long before file is deemed "OLD" in seconds
OLDTIME=-1

# Get current and file times
CURTIME=$(date +%s)
REFFILETIME=$(stat $REFFILE -c %Y)
FILETIME=$(stat $FILE -c %Y)
TIMEDIFF=$(expr $REFFILETIME - $FILETIME)

# Debug
echo $CURTIME $REFFILETIME $FILETIME


# Check if file older$TIMEDIFF 
echo TIMEDIFF is $TIMEDIFF
if [ $TIMEDIFF -gt $OLDTIME ]; then
   echo "File is older, stop here!"
else 
   echo "File is recent."
   echo "Restarting EPICS Data Diode!"
   
   # INSERT DIODE RESTART CODE HERE

   cp -f $FILE $REFFILE
   touch $REFFILE  # To prevent the next restart!
fi



